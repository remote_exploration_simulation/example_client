import asyncio
import aiofiles
import websockets as ws
import sys
import math
import random
import os

from satellite_network_protocol.src.FlagsMask import FlagsMask
import satellite_network_protocol.src.package as package
from satellite_network_protocol.src.package import PAYLOAD_SIZE
import satellite_network_protocol.src.utilities as snpUtils
from client.Buffer import Buffer


class Buffer:
    def __init__(self, contentType, senderType):
        self.contentType = contentType
        self.senderType = senderType
        self.content = []
        self.receivedLast = False


    def isMessageComplete(self):
        if not self.receivedLast:
            return False

        return None not in self.content


    def addToContent(self, number, isLast, content):
        if len(self.content) <= number:
            self.content.extend([None for _ in range(number - len(self.content) + 1)])

        self.content[number] = content

        self.receivedLast = self.receivedLast or isLast


    def getFullContent(self):
        flattened = b''
        for b in self.content:
            flattened += b

        return flattened


class MessageIdCreator:
    def __init__(self):
        self.currentId = random.randint(0, 255)

    def getNewId(self):
        old = self.currentId
        new = (old + 1) % 256
        self.currentId = new

        return old


class ExampleClient():

    def __init__(self, ip, port, token, directionType, clientType, logger):
        self.logger = logger

        self.clientID = 0
        self.clientType = clientType
        self.connected = False

        self.consumerBuffer = {}

        self.ip = ip
        self.port = port
        self.token = token

        self.directionType = directionType

        self.messageIdCreator = MessageIdCreator()

        self.bufferDir = './tmp_buffer'


    async def run(self):
        uri = f'ws://{self.ip}:{self.port}'
        async with ws.connect(uri) as conn:
            # Send initial connection request
            await conn.send(snpUtils.buildCtrlConnReq(self.token))

            # Get either own ID or get connection refused message from server
            msg = await conn.recv()
            pkg = package.Package(msg)
            self.consumeCtrlPkg(pkg)

            if not self.connected:
                return

            if self.directionType == 'prod':
                await asyncio.gather(*[
                    self.producer(conn),
                    self.consumeOnlyCtrl(conn)])
            elif self.directionType == 'cons':
                await self.consumer(conn)
            else:
                print('Incorrect type!')


    async def ainput(self, string):
        await asyncio.get_event_loop().run_in_executor(
                None, lambda s=string: sys.stdout.write(s+'\n'))
        stdinInput = await asyncio.get_event_loop().run_in_executor(
                None, sys.stdin.readline)
        return stdinInput[:-1]


    def buildPackageAuto(self, msg, msgType, messageID, segment):
        last = (segment+1)*PAYLOAD_SIZE >= len(msg)

        payload = msg[segment*PAYLOAD_SIZE:(segment+1)*PAYLOAD_SIZE]

        return snpUtils.buildStdPackage(
                payload=payload,
                payloadType=msgType,
                senderID=self.clientID,
                senderType=self.clientType,
                messageID = messageID,
                number = segment,
                last=last)


    # Sending

    async def sendText(self, msgStr, conn):
        msgId = self.messageIdCreator.getNewId()
        msgBytes = bytes(msgStr, 'utf-8')
        for seg in range(math.ceil(len(msgBytes) / PAYLOAD_SIZE)):
            package = self.buildPackageAuto(msgBytes, 'txt', msgId, seg)
            await conn.send(package)


    async def sendImg(self, imgPath, conn):
        msgID = self.messageIdCreator.getNewId()
        num = 0
        async with aiofiles.open(imgPath, 'rb') as fl:
            section = await fl.read(PAYLOAD_SIZE)
            while len(section) == PAYLOAD_SIZE:
                package = snpUtils.buildStdPackage(
                        payload=section,
                        payloadType='img',
                        senderID=self.clientID,
                        senderType=self.clientType,
                        messageID=msgID,
                        number=num,
                        last=False)
                await conn.send(package)
                num += 1
                section = await fl.read(PAYLOAD_SIZE)

            package = snpUtils.buildStdPackage(
                    payload=section,
                    payloadType='img',
                    senderID=self.clientID,
                    senderType=self.clientType,
                    messageID=msgID,
                    number=num,
                    last=True)
            await conn.send(package)


    # Receiving

    def receivedTxt(self, txt, senderID, senderType):
        print(f'[{senderID} ({senderType})] txt:\n{txt}')


    def receivedImg(self, imgPath, senderID, senderType):
        print(f'[{senderID} ({senderType})] img:\nSave image to "{imgPath}"')


    def consumeCtrlPkg(self, pkg):
        # Check if actually control message
        if not pkg.isControl:
            return

        # If package is not from the server cancel
        if pkg.senderID != 0:
            return

        payload = pkg.payload

        # Connection refused from server
        if payload[0] == 0:
            # Close connection
            print(f'Connection was refused with message: {pkg.payload[1:]}')
            self.connected = False

        # Receive new ID
        elif payload[0] == 1:
            self.connected = True
            self.clientID = int.from_bytes(payload[1:3], byteorder='big')
            print('Received new id:', str(self.clientID))


    def consumeStdPkg(self, pkg):
        key = (pkg.senderID, pkg.messageID)
        # If incorrect payload type in buffer reset it
        senderBuffer = self.consumerBuffer.get(key, None)
        if senderBuffer is None or senderBuffer.contentType != pkg.payloadType:
            self.consumerBuffer[key] = Buffer(pkg.payloadType, pkg.senderType)
            self.logger.info('Only partial package received. Got dropped.')

        self.consumerBuffer[key].addToContent(
                number=pkg.number,
                isLast=pkg.isLast,
                content=pkg.payload)

        if self.consumerBuffer[key].isMessageComplete():
            if self.consumerBuffer[key].contentType == 'txt':
                contentAsBytes = self.consumerBuffer[key].getFullContent()
                text = contentAsBytes.decode('utf-8')
                self.receivedTxt(text, pkg.senderID, pkg.senderType)

            elif self.consumerBuffer[key].contentType == 'img':
                contentAsBytes = self.consumerBuffer[key].getFullContent()
                imgPath = self.getImgPath(pkg.senderID)
                with open(imgPath, 'wb') as fl:
                    fl.write(contentAsBytes)
                self.receivedImg(imgPath, pkg.senderID, pkg.senderType)

            else:
                self.logger.warn('Received package of unknow type. Got dropped.')

            self.consumerBuffer.pop(key)


    def consumeTxtPkg(self, pkg):
        # If incorrect payload type in buffer clear it
        senderBuffer = self.clientsBuffers.get(pkg.senderID, None)
        if senderBuffer is not None and senderBuffer.contentType != 'txt':
            self.clientsBuffers[pkg.senderID] = None

        # If first package create new buffer and save payload
        if pkg.isFirst:
            self.clientsBuffers[pkg.senderID] = Buffer(pkg.payloadType, pkg.senderType)
            self.clientsBuffers[pkg.senderID].content = [pkg.payload]

        # If not first package and buffer exists append payload to the buffer
        elif self.clientsBuffers[pkg.senderID] is not None:
            self.clientsBuffers[pkg.senderID].content.append(pkg.payload)

        # If the package is the last one and a buffer exists, merge the buffer
        # into a continuous string and do something with the message
        if pkg.isLast and self.clientsBuffers[pkg.senderID] is not None:
            bufferedContent = self.clientsBuffers[pkg.senderID].content

            text = ''.join([seg.decode('utf-8') for seg in bufferedContent])
            self.receivedTxt(text, pkg.senderID, pkg.senderType)

            self.clientsBuffers.pop(pkg.senderID)


    def consumeImgPkg(self, pkg):

        # If incorrect payload type in buffer clear it
        senderBuffer = self.clientsBuffers.get(pkg.senderID, None)
        if senderBuffer is not None and senderBuffer.contentType != 'img':
            self.clientsBuffers[pkg.senderID] = None

        # If first package create new buffer, save img path and
        if pkg.isFirst:
            self.clientsBuffers[pkg.senderID] = Buffer(pkg.payloadType, pkg.senderType)
            self.clientsBuffers[pkg.senderID].content = f'./output_img_sender_{pkg.senderID}.jpeg'

            # Clear file
            with open(self.clientsBuffers[pkg.senderID].content, 'wb') as fl:
                fl.write(b'')

        # If a buffer for the current package exists, write to specified file
        if self.clientsBuffers[pkg.senderID] is not None:
            with open(self.clientsBuffers[pkg.senderID].content, 'ab') as imgFile:
                imgFile.write(pkg.payload)

            if pkg.isLast:
                self.receivedImg(self.clientsBuffers[pkg.senderID].content, pkg.senderID, pkg.senderType)

                self.clientsBuffers.pop(pkg.senderID)


    def consumePackage(self, pkg):
        """
        if pkg.isControl:
            self.consumeCtrlPkg(pkg)
        elif pkg.payloadType == 'txt':
            self.consumeTxtPkg(pkg)
        elif pkg.payloadType == 'img':
            self.consumeImgPkg(pkg)
        """
        if pkg.isControl:
            self.consumeCtrlPkg(pkg)
        else:
            self.consumeStdPkg(pkg)


    async def consumer(self, conn):
        async for retMsg in conn:
            pkg = package.Package(retMsg)
            self.consumePackage(pkg)

            if not self.connected:
                return


    async def consumeOnlyCtrl(self, conn):
        async for msg in conn:
            pkg = package.Package(msg)
            if pkg.isControl:
                self.consumeCtrlPkg(pkg)

                if not self.connected:
                    return

            await asyncio.sleep(0)


    async def producer(self, conn):
        while self.connected:
            pkgType = await self.ainput('type:')
            if pkgType == 'txt':
                payload = await self.ainput('payload (text):')
                await self.sendText(payload, conn)
            elif pkgType == 'img':
                imgPath = await self.ainput('image path:')
                await self.sendImg(imgPath, conn)
            else:
                print('Not a valid type!')


    # Other Utilities

    def getImgPath(self, senderID):
        return os.path.join(self.bufferDir, 'img_' + str(senderID) + '.jpg')
