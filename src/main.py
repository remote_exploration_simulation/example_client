import asyncio
import environs
import sys
import logging

from client.exampleClient import ExampleClient


def getConnInfoOrExit():
    env = environs.Env()
    env.read_env()
    try:
        port = env('PORT')
    except environs.EnvError:
        print('Set environment variable PORT in order to connect!', file=sys.stderr)
        sys.exit(1)

    try:
        ip = env('IP')
    except environs.EnvError:
        print('Set environment variable IP in order to connect!', file=sys.stderr)
        sys.exit(1)

    try:
        token = env('CONN_TOKEN')
    except environs.EnvError:
        print('Set environment variable CONN_TOKEN in order to connect!', file=sys.stderr)
        sys.exit(1)

    return ip, port, token


def getLogger():
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)s: %(message)s',
            handlers=[handler])
    return logging.getLogger(__name__)

def startClient():
    # Execution
    if len(sys.argv) < 3:
        print(f"Usage: {sys.argv[0]} <prod/cons>  <ground/rover>", file=sys.stderr)
        sys.exit(1)

    if sys.argv[1] not in ['cons', 'prod']:
        print(f"Usage: {sys.argv[0]} <prod/cons>  <ground/rover>", file=sys.stderr)
        sys.exit(1)

    if sys.argv[2] not in ['ground', 'rover']:
        print(f"Usage: {sys.argv[0]} <prod/cons>  <ground/rover>", file=sys.stderr)
        sys.exit(1)

    directionType = sys.argv[1]
    clientType = sys.argv[2]

    ip, port, token = getConnInfoOrExit()
    logger = getLogger()
    exampleClient = ExampleClient(
            ip,
            port,
            token,
            directionType,
            clientType,
            logger)

    asyncio.run(exampleClient.run())


if __name__ == '__main__':
    startClient()
