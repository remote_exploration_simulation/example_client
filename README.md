# Example Client

## About

This CLI Client can be used to test other clients or the server. It also demonstrates how to interface with the server and how to process incoming and outgoing packages.


## Setup

1. Clone the repository and it's submodule with either SSH:
    ```sh
    git clone --recurse-submodules git@gitlab.com:remote_exploration_simulation/example_client.git
    ```
    Or HTTPs:
    ```sh
    git clone --recurse-submodules https://gitlab.com/remote_exploration_simulation/example_client.git`
    ```

2. Enter repository directory. Then initialize a virtual environment and enter it:
    ```sh
    cd example_client
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install required packages
    ```sh
    pip3 install -r requirements.txt
    ```

4. Create `.env` file to store the server IP, the Port of the server and the token of the channel that you want to connect to.
    ```sh
    touch .env
    echo "IP = <Server IPv4>" >> .env
    echo "PORT = <Server Port Number>" >> .env
    echo "CONN_TOKEN = <Token of channel you want to connect to>" >> .env
    ```


## Starting the client

In order to start the client simply run the following command and add 'prod' or 'cons' at the end with a space, to specify if the client should be consuming messages or producing them.
```sh
python3 src/main.py <prod/cons>
```


## Stopping the client

The client runs inefinitely and can, for now, only be forcefully stopped by pressing `Ctrl`+`C`.
